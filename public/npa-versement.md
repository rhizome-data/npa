class: center, top
background-image: url(images/logo-rhizome-data.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.footnote[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data]

### Les nouvelles pratiques archivistiques du versement

---
name: programme

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

---
## Objectifs

- Les méthodes et outils d'évaluation de la production
- Les méthodes et outils de la structuration de la production
- Comment effectuer un versement unitaire ?
- Modéliser les versements sériels : architecture et protocoles

---
### Les méthodes et outils d'évaluation de la production

- Comprendre le [contexte](#contexte) de production.
- Les éléments de [vigilance](#vigilance) pour les producteur.rice.s.
- Analyse et description avec [Archifiltre](#archifiltre).
- Maîtriser la [création](#creation) de profil SEDA.
---

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme)]

---
name: contexte

### Le contexte de production

---
### Typologies de production

---

### Méthodes d'analyse

---
### Les éléments de vigilance pour les producteur.rice.s


---
### Charte de nommage

---
### Profondeur d'arborescence

---
### Formats de fichiers

---
### Analyse et description avec Archifiltre

---
### Installation et fonctionnalités

---
### Modalités de description

---
### Fonctionnalités d'export

---
### Rapport d'audit

---
### Vers le tableau de gestion et le modèle de versement


---
### Comment effectuer un versement unitaire

---
### Modéliser les versements sériels : architecture et protocoles

---
### Identifier les flux de production

---
### Identifier les zones de production des données essentielles


---
### Le standard d'échange des données d'archivage


---
### Un bordereau mis à nu

---
### Demandes (communication, élimination, restitution) et réponses


---
### A la recherche du meilleur profil

---
### Et si on indexait 

---
### Et dans quoi on verse ?

---
### L'organisation de la conservation numérique

---
