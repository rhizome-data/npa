# npa

Ce dépôt vise à construire un ensemble de ressources pédagogique visant à favoriser le développement des nouvelles pratiques archivistiques

## Contribution

Pour y contribuer il faut avoir un compte sur le site [gitlab.com](https://gitlab.com/users/sign_in).
Une fois le compte créé il est possible de passer l'interface en français en allant dans le menu ["settings"](https://gitlab.com/profile/preferences) en haut à droite (section Localization).

### Contribution en ligne

Il est possible de contribuer sous la forme de pages en markdown depuis le service en ligne [stackedit](https://stackedit.io). Il faut toutefois au préalable paramétrer (autoriser) la communication entre les 2 services.

POur cela il faut se rendre dans le menu profil de l'utilisateur.rice créé.e précédemment et dans le sous menu application à gauche ajouter une application du nom de stackedit en lui donnant les droits d'accès à l'api de Gitlab et d'écriture sur le dépôt. Un identifiant d'application est généré qu'il faut copier dans le menu des options de publications dans stackedit.
En utilisant le menu "manage workspace" il est possible de consulter l'ensemble des fichiers contenus dans le dépôt sans quitter stackedit et de les modifier depuis cette interface.

Il est également possible de modifier directement les contenus depuis le site gitlab.com mais ce n'est pas conseillé :)

### Contribution hors ligne

Pour travailler hors ligne sur le dépôt de ressources il faut au préalable en avoir effectué une copie.
Cette opération s'appelle "cloner le dépôt"

L'adresse du dépôt est accessible sur la page d'accueil sous la forme d'un lien de type git@gitlab.com:nom-du-depot-a-remplacer.git ou https://gitlab.com/nom-du-depot-a-remplacer.git

Afin d'effectuer votre copie locale vous pouvez :
* utiliser votre terminal préféré
* utiliser un éditeur de code disposant d'un gestionnaire de source (par exemple [vs codium](https://vscodium.com/))
* installer l'outil [github desktop](https://desktop.github.com/) qui fonctionne aussi bien pour github que gitlab

Dans tous les cas la commande à effectuer est 

```git
git clone https://gitlab.com/nom-du-depot-a-remplacer.git
```
Cette opération va effectuer une copie des fichiers et dossiers stockés sur gitlab.com dans l'arborescence de votre disque dur à l'endroit où vous avez lancé la commande ou à l'endroit que vous avez spécifié si vous avez utilisé un outil.

A partir de ce moment vous pouvez effectuer des modifications en local et les synchroniser avec le contenu stocké sur gitlab.com au moyen de diverses commande disponible dans le protocole git.

Par exemple ajouter un fichier modifié localement se dit : 

```
git add monfichier.md
git commit -m 'ajout d'un fichier'
git push
```

Avant d'effectuer ces opérations il est prudent d'effectuer au préalable un 

```
git fetch
ou
git pull
```
afin de récupérer les dernières modifications qui auraient pu être faites par d'autres contributeur.rice.s depuis votre dernière synchronisation

Si vous utilisez la ligne de commande ou github desktop pour synchoniser vos modifications locales avec le dépôt distant vous pouvez utiliser un outil comme [typora](https://typora.io/) pour écire du markdown sans le savoir :)

Pour l'utilisation de gitlab et l'appréhension des commandes courantes du protocole git, je vous recommande ce [tutoriel](https://github.com/SocialGouv/tutoriel-gitlab) 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTMyNTY4Njc3MV19
-->